#language: pt
#encoding: utf-8
@chrome
Funcionalidade: Tela de pesquisa - Ver Laudo

  @quarentena
  Cenario: Ver series do exame
    Dado que realizo o login na tela de pesquisa
    E pesquiso pelo id do paciente "179907"
    E clico em pesquisar
    E seleciono o exame "179907" na grid
    Quando clico em ver laudo
    Entao deve ser exibida o report do laudo
    Quando clico em fechar laudo
    Entao o report do laudo deve ser encerrado
    