package steps;

import org.junit.Assert;

import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pages.FiltrosPage;
import pages.VerLaudoPage;

public class VerLaudoSteps {

	@Quando("^clico em ver laudo$")
	public void clicarVerLaudo() {
		FiltrosPage f = new FiltrosPage();
		f.clicarVerLaudo();
	}

	@Entao("^deve ser exibida o report do laudo$")
	public void deveSerExibidaReportLaudo() {
		VerLaudoPage laudo = new VerLaudoPage();
		Assert.assertTrue(laudo.deveSerExibidaReportLaudo());
	}

	@Quando("^clico em fechar laudo$")
	public void clicarFecharLaudo() {
		VerLaudoPage laudo = new VerLaudoPage();;
		laudo.clicarFecharLaudo();
	}

	@Entao("^o report do laudo deve ser encerrado$")
	public void reportLaudoDeveSerEncerrado() {
		VerLaudoPage laudo = new VerLaudoPage();
		Assert.assertTrue(laudo.reportLaudoDeveSerEncerrado());
	}

}
