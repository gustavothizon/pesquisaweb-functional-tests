#language: pt
#encoding: utf-8
@chrome
Funcionalidade: Tela de pesquisa

  @quarentena
  Cenario: Realizar pesquisa pelo nome do paciente
    Dado que realizo o login na tela de pesquisa
    E pesquiso pelo nome do paciente "TESTE"
    Quando clico em pesquisar
    Entao deve existir o item "TESTE" na grid de exames pesquisados

  @quarentena
  Cenario: Realizar pesquisa pela descricao do exame
    Dado que realizo o login na tela de pesquisa
    E pesquiso pela descricao do exame "TESTE"
    Quando clico em pesquisar
    Entao deve existir o item "TESTE" na grid de exames pesquisados

  @quarentena
  Cenario: Realizar pesquisa pelo numero de acesso
    Dado que realizo o login na tela de pesquisa
    E pesquiso pelo numero de acesso "410192"
    Quando clico em pesquisar
    Entao deve existir o item "Torax Infantil" na grid de exames pesquisados

  @quarentena
  Cenario: Realizar pesquisa pela modalidade do exame
    Dado que realizo o login na tela de pesquisa
    E pesquiso pela modalidade "MR"
    Quando clico em pesquisar
    Entao deve existir o item "MR" na grid de exames pesquisados

  @quarentena
  Cenario: Realizar pesquisa pelo id do paciente
    Dado que realizo o login na tela de pesquisa
    E pesquiso pelo id do paciente "10"
    Quando clico em pesquisar
    Entao deve existir o item "TM" na grid de exames pesquisados

