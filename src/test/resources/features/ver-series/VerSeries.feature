#language: pt
#encoding: utf-8
@chrome
Funcionalidade: Tela de pesquisa - Ver series

  @quarentena
  Cenario: Ver series do exame
    Dado que realizo o login na tela de pesquisa
    E pesquiso pelo id do paciente "51455"
    E clico em pesquisar
    E seleciono o exame "51455" na grid
    Quando clico em ver series
    Entao deve ser exibida a janela para selecionar serie
    Quando seleciono a serie "1" da lista
    E clico em abrir serie
    E acesso a aba 2 aberta no navegador
    Entao o painel "1" do exame deve apresentar a imagem "imagem.png"