package tests;

import org.junit.ClassRule;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import util.TestRule;

@RunWith(Cucumber.class)
@CucumberOptions(features = "@target/rerun.txt", 
	  glue = {""}, monochrome = true, dryRun = false, strict = true, plugin = {"rerun:target/rerun.txt"})
public class Tentativa2Test {

	@ClassRule
	public static TestRule testRule = new TestRule();

}