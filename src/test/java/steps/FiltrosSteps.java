package steps;

import java.awt.AWTException;
import java.io.IOException;

import org.junit.Assert;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pages.BasePage;
import pages.FiltrosPage;
import pages.GenericPage;

public class FiltrosSteps {

	@E("^pesquiso pelo nome do paciente \"([^\"]*)\"$")
	public void pesquisarPeloNomePaciente(String nome) {
		FiltrosPage filtro = new FiltrosPage();
		filtro.pesquisarPeloNomePaciente(nome);
	}

	@Quando("^clico em pesquisar$")
	public void clicarEmPesquisar() {
		FiltrosPage filtro = new FiltrosPage();
		filtro.clicarEmPesquisar();
	}

	@Entao("^deve existir o item \"([^\"]*)\" na grid de exames pesquisados$")
	public void deveExistirItemGridExamesPesquisados(String item) throws AWTException {
		FiltrosPage filtro = new FiltrosPage();
		Assert.assertTrue(filtro.deveExistirItemGridExamesPesquisados(item));
	}
	
	@E("^pesquiso pela descricao do exame \"([^\"]*)\"$")
	public void pesquisarPelaDescricaoExame(String descricao) {
		FiltrosPage filtro = new FiltrosPage();
		filtro.pesquisarPelaDescricaoExame(descricao);
	}

	@E("^pesquiso pelo numero de acesso \"([^\"]*)\"$")
	public void pesquisarPeloNumeroAcesso(String numeroAcesso) {
		FiltrosPage filtro = new FiltrosPage();
		filtro.pesquisarPeloNumeroAcesso(numeroAcesso);
	}

	@E("^pesquiso pela modalidade \"([^\"]*)\"$")
	public void pesquisarPelaModalidade(String modalidade) {
		FiltrosPage filtro = new FiltrosPage();
		filtro.pesquisarPelaModalidade(modalidade);
	}

	@E("^pesquiso pelo id do paciente \"([^\"]*)\"$")
	public void pesquisarPeloIdPaciente(String paciente) {
		FiltrosPage filtro = new FiltrosPage();
		filtro.pesquisarPeloIdPaciente(paciente);
	}
	
	@Quando("^seleciono o exame \"([^\"]*)\" na grid$")
	public void selecionarExameNaGrid(String exame) {
		FiltrosPage filtro = new FiltrosPage();
		filtro.selecionarExameNaGrid(exame);
	}

	@Quando("^clico em abrir exame$")
	public void clicarAbrirExame() {
		FiltrosPage filtro = new FiltrosPage();
		filtro.clicarAbrirExame();
	}

	@Entao("^o painel \"([^\"]*)\" do exame deve apresentar a imagem \"([^\"]*)\"$")
	public void painelExameDeveApresentarImagem(String painel, String imagem) throws IOException {
		GenericPage generic = new GenericPage();
		Assert.assertTrue(generic.elementodeveApresentarImagem("canvas_"+painel, imagem));
	}
	
	@Quando("^acesso a aba (\\d+) aberta no navegador$")
	public void acessarAbaAbertaNavegador(int aba) {
		BasePage base = new BasePage();
		base.changeTabBrowser(aba-1);
	}



}
