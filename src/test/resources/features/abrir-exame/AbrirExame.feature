#language: pt
#encoding: utf-8
@chrome
Funcionalidade: Tela de pesquisa - Abrir exame

  @quarentena
  Cenario: Abrir exame
    Dado que realizo o login na tela de pesquisa
    E pesquiso pelo id do paciente "51455"
    E clico em pesquisar
    E seleciono o exame "51455" na grid
    E clico em abrir exame
    Quando acesso a aba 2 aberta no navegador
    Entao o painel "1" do exame deve apresentar a imagem "imagem.png"