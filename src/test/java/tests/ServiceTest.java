package tests;

import org.junit.ClassRule;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import util.TestRule;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:features", tags = {"@service", "~@quarentena"},
	  glue = {""}, monochrome = true, dryRun = false, plugin = {"json:target/cucumber.json","rerun:target/rerun.txt"})
public class ServiceTest {

	@ClassRule
	public static TestRule testRule = new TestRule();

}