package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPageElementMap extends BasePage {
	
	@FindBy(id = "loadingSvg")
	protected WebElement loadPage;

	@FindBy(id = "inputLoginUsername")
	protected WebElement campoUsuario;
	
	@FindBy(id = "inputLoginPassword")
	protected WebElement campoSenha;

	@FindBy(id = "loginButton")
	protected WebElement botaoEntrar;

	@FindBy(xpath = "//*[contains(@class,'main-title') and contains(text(),'Pesquisa')]")
	protected WebElement telaPesquisaPrincipal;
	
}
