package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;

import util.TestRule;

public class VerLaudoPage extends VerLaudoPageElementMap {

	public VerLaudoPage() {
		PageFactory.initElements(TestRule.getDriver(), this);
	}

	public boolean deveSerExibidaReportLaudo() {
		waitElement(botaoFecharLaudo, 5);
		String url = driver.getCurrentUrl();
		if (url.contains("search/report_viewer")) {
			logPrint("Report foi exibido corretamente");
			return true;
		}
		logPrint("Report nao foi exibido");
		return false;
	}

	public void clicarFecharLaudo() {
		waitElement(botaoFecharLaudo, 5);
		botaoFecharLaudo.sendKeys(Keys.RETURN);
		logPrint("Clicou para fechar o laudo");
	}

	public boolean reportLaudoDeveSerEncerrado() {
		waitNotPresent(botaoFecharLaudo, 10);
		String url = driver.getCurrentUrl();
		if (!url.contains("report_viewer")) {
			logPrint("Report foi encerrado corretamente");
			return true;
		}
		logPrint("Report nao foi encerrado");
		return false;
	}

}
