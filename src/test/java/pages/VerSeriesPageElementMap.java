package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class VerSeriesPageElementMap extends BasePage {

	@FindBy(className = "modal-series")
	protected WebElement modalSelecionarSeries;

	@FindBy(id = "pixeonGridSeriesRow0")
	protected WebElement selecionarSerie;

	@FindBy(xpath = "//button[@id = 'openXviewerSerie' and not (@disabled = 'disabled')]")
	protected WebElement botaoAbrirSerieXviewer;
	
	protected WebElement selecionarSerieLista(String serie) {
		WebElement serieSelecionada = driver.findElement(By.xpath("//div[@id = 'pixeonGridSeriesRow"+ serie +"']/div[3]/div"));
		return serieSelecionada;
	}
}
