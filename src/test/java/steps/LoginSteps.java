package steps;

import cucumber.api.java.pt.Dado;
import pages.LoginPage;
import util.TestRule;
import util.Utils;

public class LoginSteps {
	
	@Dado("^que realizo o login na tela de pesquisa$")
	public void acessarTelaPesquisa() {
		TestRule.openApplicationChrome(Utils.getTestProperty("url"));
		LoginPage login = new LoginPage();
		login.realizarLogin();
	}

}
