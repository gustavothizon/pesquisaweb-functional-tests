/***
Run all suit of functional tests every night
***/

pipeline {
    agent { label 'master' }
    options {
        disableConcurrentBuilds()
        skipDefaultCheckout()
        buildDiscarder(logRotator(numToKeepStr: '20'))
        timeout(time: 6, unit: 'HOURS')
    }
    triggers {
    	cron('H 22,4 * * 1-5')
    }
	environment {
	    NEXUS = credentials('nexus-deploy')
	}

    stages {
        stage("Deploy Environments"){
	        parallel {
	            stage('pacs-sqlserver2017') {
	            	agent { label 'vagrant-host' }
	                steps {
	                    bat """
	                    	P:
							cd P:\\vagrant\\sqlserver2017
							vagrant halt --force
							vagrant snapshot restore automacao2
							exit 0
							"""
						bat """
							c:
							cd /
							curl -X GET -u $NEXUS_USR:$NEXUS_PSW \"https://nexus.pixeon.cloud/repository/arya/19.5.0-release_19.5.0/052arya_native-19.5.0-release_19.5.0.macosx.zip\" -O
							curl -X GET -u $NEXUS_USR:$NEXUS_PSW \"https://nexus.pixeon.cloud/repository/arya/19.5.0-release_19.5.0/052arya_native-19.5.0-release_19.5.0.ubuntu_16.04.zip\" -O
							curl -X GET -u $NEXUS_USR:$NEXUS_PSW \"https://nexus.pixeon.cloud/repository/arya/19.5.0-release_19.5.0/052arya_native-19.5.0-release_19.5.0.win32.zip\" -O
							curl -X GET -u $NEXUS_USR:$NEXUS_PSW \"https://nexus.pixeon.cloud/repository/arya/19.5.0-release_19.5.0/052arya_native-19.5.0-release_19.5.0.win64.zip\" -O
							curl -X GET -u $NEXUS_USR:$NEXUS_PSW \"https://nexus.pixeon.cloud/repository/arya/19.5.0-release_19.5.0/arya_svm-19.5.0-release_19.5.0.zip\" -O
							curl -X GET -u $NEXUS_USR:$NEXUS_PSW \"https://nexus.pixeon.cloud/repository/arya/19.5.0-release_19.5.0/extra_files-19.5.0-release_19.5.0.zip\" -O
							curl -X GET -u $NEXUS_USR:$NEXUS_PSW \"https://nexus.pixeon.cloud/repository/arya/19.5.0-release_19.5.0/19.5.0-release_19.5.0\" -O
							del /Q /F current
							ren 19.5.0-release_19.5.0 current
							powershell -Command \"(gc current) -replace 'pixviewer/.*.jar', 'viewerj/pixviewerj.jar' | Out-File current\" -Encoding \"UTF8\"

							pscp.exe -pw vagrant 052arya_native-19.5.0-release_19.5.0.macosx.zip vagrant@win-h490hff654q:C:\\jenkins-slave\\workspace\\automacao\\pacs-refactoring\\lib\\zipper\\target\\install\\aurora\\server\\local\\deploy\\jboss-web.deployer\\ROOT.war\\apps\\arya\\052arya_native-19.5.0-release_19.5.0.macosx.zip
							pscp.exe -pw vagrant 052arya_native-19.5.0-release_19.5.0.ubuntu_16.04.zip vagrant@win-h490hff654q:C:\\jenkins-slave\\workspace\\automacao\\pacs-refactoring\\lib\\zipper\\target\\install\\aurora\\server\\local\\deploy\\jboss-web.deployer\\ROOT.war\\apps\\arya\\052arya_native-19.5.0-release_19.5.0.ubuntu_16.04.zip
							pscp.exe -pw vagrant 052arya_native-19.5.0-release_19.5.0.win32.zip vagrant@win-h490hff654q:C:\\jenkins-slave\\workspace\\automacao\\pacs-refactoring\\lib\\zipper\\target\\install\\aurora\\server\\local\\deploy\\jboss-web.deployer\\ROOT.war\\apps\\arya\\052arya_native-19.5.0-release_19.5.0.win32.zip
							pscp.exe -pw vagrant 052arya_native-19.5.0-release_19.5.0.win64.zip vagrant@win-h490hff654q:C:\\jenkins-slave\\workspace\\automacao\\pacs-refactoring\\lib\\zipper\\target\\install\\aurora\\server\\local\\deploy\\jboss-web.deployer\\ROOT.war\\apps\\arya\\052arya_native-19.5.0-release_19.5.0.win64.zip
							pscp.exe -pw vagrant arya_svm-19.5.0-release_19.5.0.zip vagrant@win-h490hff654q:C:\\jenkins-slave\\workspace\\automacao\\pacs-refactoring\\lib\\zipper\\target\\install\\aurora\\server\\local\\deploy\\jboss-web.deployer\\ROOT.war\\apps\\arya\\arya_svm-19.5.0-release_19.5.0.zip
							pscp.exe -pw vagrant extra_files-19.5.0-release_19.5.0.zip vagrant@win-h490hff654q:C:\\jenkins-slave\\workspace\\automacao\\pacs-refactoring\\lib\\zipper\\target\\install\\aurora\\server\\local\\deploy\\jboss-web.deployer\\ROOT.war\\apps\\arya\\extra_files-19.5.0-release_19.5.0.zip
							pscp.exe -pw vagrant current vagrant@win-h490hff654q:C:\\jenkins-slave\\workspace\\automacao\\pacs-refactoring\\lib\\zipper\\target\\install\\aurora\\server\\local\\deploy\\jboss-web.deployer\\ROOT.war\\descriptor\\arya\\current
						    """
	                }
	            }
	            stage('client-windows10') {
	            	agent { label 'vagrant-host' }
	            	steps {
	                    bat """
							P:
							cd P:\\vagrant\\windows10-pacs-2.0.0
							vagrant halt --force
							vagrant snapshot restore automacao2
							exit 0
							"""
	                }
	            }
	        }
        }
    
        stage("Functional Test"){
	        parallel {
	            stage('arya-winx64-nodo1') {
	                agent { label 'windows10-vagrant' }
	                steps {
	                    git credentialsId: "scm_pixeon", url: "https://scm_pixeon@bitbucket.org/pixeon/arya-functional-tests.git"
	                    bat "mvn clean test -Dtest=WindowsTest -Dpacs.server=win-h490hff654q -Dpacs.user=automacao3 -Dpacs.pass=teste -Darya.descriptor=current -Dmaven.test.failure.ignore=true"
	                    bat "mvn test -Dtest=Tentativa2Test -Dpacs.server=win-h490hff654q -Dpacs.user=automacao3 -Dpacs.pass=teste -Darya.descriptor=current -Dmaven.test.failure.ignore=true -DfailIfNoTests=false"
	                    bat "mvn test -Dtest=Tentativa3Test -Dpacs.server=win-h490hff654q -Dpacs.user=automacao3 -Dpacs.pass=teste -Darya.descriptor=current -DfailIfNoTests=false"
	                }
	                post {
	                    always {
	                        archive 'target/evidencias/html/*.html,target/evidencias/html/img/*,target/pixviewerlogs/*'
	                        livingDocs featuresDir: 'target', format: 'ALL'
	                        junit 'target/surefire-reports/*.xml'
	                        cucumber fileIncludePattern: '**/*.json', jsonReportDirectory: 'target', sortingMethod: 'ALPHABETICAL'
	                    }
	                }
	            }
	            stage('arya-winx86') {
	            	steps {
	                    echo 'todo'
	                }
	            }
	            stage('arya-mac') {
	                steps {
	                    echo 'todo'
	                }
	            }
	            stage('arya-linux') {
	                steps {
	                    echo 'todo'
	                }
	            }
	        }
        }
        
        stage("Stop Environments"){
	        parallel {
	            stage('pacs-sqlserver2017') {
	            	agent { label 'vagrant-host' }
	                steps {
	                    bat """
							P:
							cd P:\\vagrant\\sqlserver2017
							vagrant halt --force
							"""
	                }
	            }
	            stage('client-windows10') {
	            	agent { label 'vagrant-host' }
	            	steps {
	                    bat """
							P:
							cd P:\\vagrant\\windows10-pacs-2.0.0
							vagrant halt --force
							"""
	                }
	            }
	        }
        }
        
    }
}