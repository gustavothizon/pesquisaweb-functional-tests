package pages;

import org.openqa.selenium.support.PageFactory;

import util.TestRule;
import util.Utils;

public class LoginPage extends LoginPageElementMap {

	public LoginPage() {
		PageFactory.initElements(TestRule.getDriver(), this);
	}
	
	public void realizarLogin() {
		waitElement(campoUsuario, 30);
		campoUsuario.sendKeys(Utils.getTestProperty("user"));
		campoSenha.sendKeys(Utils.getTestProperty("pass"));
		botaoEntrar.click();
		waitElement(telaPesquisaPrincipal, 30);
		logPrint("Abriu a tela de pesquisa");
	}
	
	public void waitLoadingSystem() {
		try {
			waitElement(loadPage, 10);
		}catch (Exception e) {
		}
		waitNotPresent(loadPage, 120);
		logPrint("Acesso realizado com sucesso");
	}

}
