package pages;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WrapsDriver;
import org.openqa.selenium.interactions.Actions;
import org.sikuli.script.Finder;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.sun.javafx.geom.Rectangle;

import util.Utils;

public class GenericPage extends GenericPageElementMap {

	public boolean deveApresentarDescricao(String descricao) {
		try {
			waitElement(retornarElementoPeloTextoInformado(descricao), 5);
			logPrint("O elemento contendo o texto " + descricao + " foi encontrado");
			return true;
		} catch (Exception e) {
			logPrintFail("Falha ao aguardar presenca do elemento com descricao : " + descricao);
			return false;
		}
	}

	public boolean elementoNaoDeveEstarVisivel(String id) {
		try {
			aguardarElementoNaoEstarVisivel(10, By.xpath("//*[@id='" + id + "']"));
			logPrint("Elemento nao presente conforme esperado");
			return true;
		} catch (Exception e) {
			logPrintFail("Falha ao aguardar elemento nao estar presente, id: " + id);
			return false;
		}
	}

	public boolean elementoDeveEstarVisivel(String id) {
		try {
			waitElement(By.xpath("//*[@id='" + id + "']"), 10);
			logPrint("Elemento visivel conforme esperado. Id: " + id);
			return true;
		} catch (Exception e) {
			logPrintFail("Elemento nao esta visivel, id:" + id);
			return false;
		}
	}

	public boolean elementodeveApresentarImagem(String id, String imagem) throws IOException {
		WebElement element = driver.findElement(By.id(id));
		takeScreenshotElement(element);

		String capturaTela = takeScreenshotElement(element);
		String imagemEsperada = "src/test/resources/comparar-imagens/" + imagem;

		return isImageEquals(imagemEsperada, capturaTela);
	}

	public String takeScreenshotElement(WebElement element) throws IOException {
		WrapsDriver wrapsDriver = (WrapsDriver) element;
		File screenshot = ((TakesScreenshot) wrapsDriver.getWrappedDriver()).getScreenshotAs(OutputType.FILE);
		Rectangle rectangle = new Rectangle(element.getSize().width, element.getSize().height);
		Point location = element.getLocation();
		BufferedImage bufferedImage = ImageIO.read(screenshot);
		BufferedImage destImage = bufferedImage.getSubimage(location.x, location.y, rectangle.width, rectangle.height);
		ImageIO.write(destImage, "png", screenshot);
		File file = new File("target/evidencias/html/img/" + "screenElement" + Utils.getDateTime("hhmmss") + ".png");
		FileUtils.copyFile(screenshot, file);
		return file.getAbsolutePath();
	}

	public boolean isImageEquals(String expectedImage, String resultImagePath) throws IOException {
		Finder resultImage = new Finder(ImageIO.read(new File(resultImagePath)));
		File file = new File(resultImagePath);
		File file2 = new File(expectedImage);

		extentTest.info("Expected image: ", MediaEntityBuilder.createScreenCaptureFromPath(file2.getAbsolutePath()).build());
		extentTest.info("Result image: ", MediaEntityBuilder.createScreenCaptureFromPath(file.getAbsolutePath()).build());
		resultImage.find(expectedImage);

		if (resultImage.hasNext()) {
			Double comparisonScore = resultImage.next().getScore();

			boolean isImageEquals = comparisonScore > 0.80;

			if (isImageEquals) {
				logPrint("Completely equal images. Measured similarity: " + comparisonScore);
				return true;
			} else {
				logPrintFail("Images not equals. Measured similarity: " + comparisonScore);
				return false;
			}
		}

		logPrintFail("Completely different images.");
		return false;
	}

	public void maximizarTela() {
		try {
			driver.manage().window().setSize(new Dimension(1616, 876));
			logPrint("Restaurou resolucao da tela");
		} catch (WebDriverException e) {
		}
	}

	public void selecionarPainel(String canvas) {
		WebElement element = driver.findElement(By.xpath("//*[@id='" + canvas + "']"));
		element.click();
		logPrint("Selecionou o elemento " + canvas);
	}

	public void posicionarNaImagem(int imagem) {
		Actions act = new Actions(driver);
		int imagemAtual = 1;
		while (imagemAtual < imagem) {
			act.sendKeys(Keys.ARROW_DOWN).perform();
			imagemAtual++;
		}
		logPrint("Posicionou na imagem " + imagem);
	}

}
