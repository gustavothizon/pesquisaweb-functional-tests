package pages;

import java.awt.AWTException;
import java.awt.Robot;

import org.openqa.selenium.support.PageFactory;

import util.TestRule;

public class VerSeriesPage extends VerSeriesPageElementMap {

	public VerSeriesPage() {
		PageFactory.initElements(TestRule.getDriver(), this);
	}

	public void clicarVerSeries() {
		FiltrosPage f = new FiltrosPage();
		waitElement(f.botaoVerSeries, 10);
		f.botaoVerSeries.click();
		logPrint("Clicou no botao ver series");
	}

	public boolean deveSerExibidaJanelaSelecionarSerie() {
		try {
			waitElement(modalSelecionarSeries, 10);
			logPrint("Abriu a tela para selecionar serie");
			return true;
		} catch (Exception e) {
			logPrint("Tela selecionar serie nao foi exibida");
			return false;
		}
	}

	public void selecionarSerie(String serie) throws InterruptedException, AWTException {
//		Actions action = new Actions(driver);
		selecionarSerie.click();
		Thread.sleep(1500);
		Robot robot = new Robot();
		robot.mouseMove(500, 500);
		logPrint("Selecionou a serie " + serie);
	}

	public void clicarAbrirSerie() {
		waitElement(botaoAbrirSerieXviewer, 5);
		botaoAbrirSerieXviewer.click();
		logPrint("Clicou para abrir a serie selecionada");
	}

}
