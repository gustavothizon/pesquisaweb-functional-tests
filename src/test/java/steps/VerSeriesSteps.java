package steps;

import java.awt.AWTException;

import org.junit.Assert;

import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pages.VerSeriesPage;

public class VerSeriesSteps {

	@Quando("^clico em ver series$")
	public void clicarVerSeries() {
		VerSeriesPage ver = new VerSeriesPage();
		ver.clicarVerSeries();
	}

	@Entao("^deve ser exibida a janela para selecionar serie$")
	public void deveSerExibidaJanelaParaSelecionarSerie() {
		VerSeriesPage ver = new VerSeriesPage();
		Assert.assertTrue(ver.deveSerExibidaJanelaSelecionarSerie());
	}

	@Quando("^seleciono a serie \"([^\"]*)\" da lista$")
	public void selecionarSerieLista(String serie) throws InterruptedException, AWTException {
		VerSeriesPage ver = new VerSeriesPage();
		ver.selecionarSerie(serie);
	}

	@Quando("^clico em abrir serie$")
	public void clicarAbrirSerie() {
		VerSeriesPage ver = new VerSeriesPage();
		ver.clicarAbrirSerie();
	}

}
