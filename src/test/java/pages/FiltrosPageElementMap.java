package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class FiltrosPageElementMap extends BasePage {

	@FindBy(id = "formPatientName")
	protected WebElement filtroNomePaciente;

	@FindBy(id = "multiSelectInsideButton")
	protected WebElement filtroModalidade;

	@FindBy(id = "inputMultiselectFilter")
	protected WebElement buscarFiltroModalidade;

	@FindBy(id = "modality0")
	protected WebElement selecionarModalidade;

	@FindBy(id = "studyDescription")
	protected WebElement filtroDescricao;

	@FindBy(id = "accessionNumber")
	protected WebElement filtroNumeroAcesso;

	@FindBy(id = "patientId")
	protected WebElement filtroIdPaciente;

	@FindBy(id = "dateOption")
	protected Select filtroData;

	@FindBy(id = "StudyDateStart")
	protected WebElement filtroDataInicial;

	@FindBy(id = "StudyDateEnd")
	protected WebElement filtroDataFinal;

	@FindBy(id = "restoreDateCombo")
	protected WebElement botaoRetornarFiltroData;

	@FindBy(id = "searchButton")
	protected WebElement botaoPesquisar;

	@FindBy(id = "formCleanButton")
	protected WebElement botaoLimparFiltros;

	@FindBy(id = "pixeonGridRow0")
	protected WebElement primeiroResultadoGridPesquisa;

	@FindBy(id = "dataViewFilter")
	protected WebElement filtroLocalizarNaLista;

	@FindBy(xpath = "//button[@id='openStudy' and not (@disabled = 'disabled')]")
	protected WebElement botaoAbrirExame;

	@FindBy(xpath = "//button[@id='openSeriesButton' and not (@disabled = 'disabled')]")
	protected WebElement botaoVerSeries;

	@FindBy(id = "openReportsButton")
	protected WebElement botaoVerLaudo;

	@FindBy(xpath = "//*[contains(@id, 'pixeonGridRow') and contains(@class,'active even')]")
	protected WebElement exameSelecionado;

	@FindBy(xpath = "//*[contains(text(), 'realizada com sucesso.')]")
	protected WebElement mensagemSucesso;

	protected WebElement primeiroResultadoGridPesquisa(String texto) {
		WebElement element = driver
				.findElement(By.xpath("//*[@id = 'pixeonGridRow0']//*[contains(text(),'" + texto + "')]"));
		return element;
	}
}
