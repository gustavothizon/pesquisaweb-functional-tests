package pages;

import java.awt.AWTException;

import org.openqa.selenium.support.PageFactory;

import util.TestRule;

public class FiltrosPage extends FiltrosPageElementMap {

	public FiltrosPage() {
		PageFactory.initElements(TestRule.getDriver(), this);
	}

	public void pesquisarPeloNomePaciente(String nome) {
		waitElement(filtroNomePaciente, 10);
		filtroNomePaciente.sendKeys(nome);
		logPrint("Informou o paciente " + nome + " para pesquisa");
	}

	public void clicarEmPesquisar() {
		botaoPesquisar.click();
		waitForElement(mensagemSucesso, 10);
		logPrint("Clicou no botao pesquisar");
	}

	public boolean deveExistirItemGridExamesPesquisados(String item) throws AWTException {
//		waitElement(botaoAbrirExame, 10);
		try {
			waitElement(primeiroResultadoGridPesquisa(item), 10);
			logPrint("Item " + item + " pesquisado com sucesso.");
			return true;
		} catch (Exception e) {
			logPrintFail("Item " + item + " nao foi listado.");
			return false;
		}
	}

	public void pesquisarPelaDescricaoExame(String descricao) {
		waitElement(filtroDescricao, 10);
		filtroDescricao.sendKeys(descricao);
		logPrint("informou a descricao '" + descricao + "' para pesquisa");
	}

	public void pesquisarPeloNumeroAcesso(String numeroAcesso) {
		waitElement(filtroNumeroAcesso, 10);
		filtroNumeroAcesso.sendKeys(numeroAcesso);
		logPrint("informou o numero de acesso " + numeroAcesso + " para pesquisa");
	}

	public void pesquisarPelaModalidade(String modalidade) {
		waitElement(filtroModalidade, 10);
		filtroModalidade.click();
		waitElement(buscarFiltroModalidade, 5);
		buscarFiltroModalidade.sendKeys(modalidade);
		waitElement(selecionarModalidade, 5);
		selecionarModalidade.click();
		logPrint("Selecionou a modalidade " + modalidade + " para pesquisa");
	}

	public void pesquisarPeloIdPaciente(String paciente) {
		waitElement(filtroIdPaciente, 10);
		filtroIdPaciente.sendKeys(paciente);
		logPrint("informou o id do paciente " + paciente + " para pesquisa");
	}

	public void selecionarExameNaGrid(String exame) {
		waitElement(primeiroResultadoGridPesquisa(exame), 10);
		primeiroResultadoGridPesquisa(exame).click();
		waitElement(exameSelecionado, 10);
		logPrint("Selecionou o exame " + exame);
	}

	public void clicarAbrirExame() {
		waitElement(botaoAbrirExame, 10);
		botaoAbrirExame.click();
		logPrint("Clicou para abrir o exame");
	}

	public void clicarVerLaudo() {
		waitElement(botaoVerLaudo, 10);
		botaoVerLaudo.click();
		logPrint("Clicou para ver laudo");
	}

}